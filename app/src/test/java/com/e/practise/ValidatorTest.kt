package com.e.practise



import com.google.common.truth.Truth.assertThat
import org.junit.Test


class ValidatorTest {

    @Test
    fun `empty username return false` (){
        val result = Validator.validateUserInput(
            "",
            "123",
            "123"
        )
        assertThat(result).isFalse()
    }


}