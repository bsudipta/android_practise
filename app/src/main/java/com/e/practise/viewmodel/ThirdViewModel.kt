package com.e.practise.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.e.practise.network.ApiService
import com.e.practise.paging.PostDataSource

class ThirdViewModel(private val apiService: ApiService) : ViewModel(){

    val listData = Pager(PagingConfig(pageSize = 6)){
        PostDataSource(apiService)
    }.flow.cachedIn(viewModelScope)
}