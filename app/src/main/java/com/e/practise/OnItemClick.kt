package com.e.practise

interface OnItemClick {
    fun onClick(name: String)
}