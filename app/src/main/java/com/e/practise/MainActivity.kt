package com.e.practise

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.e.practise.adapter.MyOneAdapter
import com.e.practise.contract.OnItemClickOne
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.play.core.review.ReviewManagerFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.filter_chip.*

data class User(
    val name: String? = null
)

class MainActivity : AppCompatActivity() , OnItemClick, OnItemClickOne{

    private var myAdapter: MyAdapter? = null
    private var myOneAdapter: MyOneAdapter? = null

    private var selectedList  = mutableListOf<String>()
    private var selectedListOne  = mutableListOf<String>()

    private val PEOPLE = listOf("John Smith", "Kate Eckhart", "Emily Sun", "Frodo Baggins")

    private val _users = mutableListOf<User>()
    val users: List<User> get() = _users

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        _users.map { user ->
            if (user.name != null){
                "${user.name}"
            }else{
                user.name ?: "Unknown"
            }
        }

        //
        val manager = ReviewManagerFactory.create(this@MainActivity)
        val request = manager.requestReviewFlow()

        request.addOnCompleteListener { request ->
            if (request.isSuccessful){
                val info = request.result
                val flow = manager.launchReviewFlow(this,info)
                flow.addOnCompleteListener {

                }
            }else{
                Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
            }
        }




        myAdapter = MyAdapter(getList(), this)
        rvList.apply {
            adapter = myAdapter
        }

        myOneAdapter = MyOneAdapter(getList(),this)
        rvListOne.apply {
            adapter = myAdapter
        }

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (s!!.isNotEmpty()) {
                    filterSearch(s.toString())
                    rvList.visibility = View.VISIBLE
                } else {
                    rvList.visibility = View.INVISIBLE
                }
            }

            override fun afterTextChanged(p0: Editable?) {

            }

        })

        val adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_dropdown_item_1line,
            PEOPLE
        )

        auto?.setAdapter(adapter)
        auto?.setOnItemClickListener { adapterView, view, i, l ->

            auto?.text = null

            val selected = adapterView.getItemAtPosition(i) as String

            if (isExistingItemOne(selected)){
                Toast.makeText(this, "Item already added", Toast.LENGTH_SHORT).show()
            }else{
                selectedListOne.add(selected)
                addNewChip(selected,chipgroup)
            }

        }

        /*etSearchOne.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (s!!.isNotEmpty()) {
                    filterSearch(s.toString())
                    rvListOne.visibility = View.VISIBLE
                } else {
                    rvListOne.visibility = View.INVISIBLE
                }
            }

            override fun afterTextChanged(p0: Editable?) {

            }

        })*/


    }

    private fun filterSearch(text: String) {
        val dataList:ArrayList<Data> = ArrayList()
        for (i in getList()){
            if (i.name?.toLowerCase()!!.contains(text.toLowerCase())){
                dataList.add(i)
            }
        }
        myAdapter?.filterList(dataList)

    }

    override fun onClick(text: String) {
        if (isExistingItem(text)){
            Toast.makeText(this, "Item already Added", Toast.LENGTH_SHORT).show()
        }else{
            selectedList.add(text)
            addNewChip(text, flexLayout)
        }

    }

    override fun onClickOne(name: String) {
        if (isExistingItemOne(name)){
            Toast.makeText(this, "Item already Added", Toast.LENGTH_SHORT).show()
        }else{
            selectedListOne.add(name)
            addNewChip(name, chipgroup)
        }
    }

    fun isExistingItem(key: String): Boolean{
        var status = false
        selectedList.forEach {
            if (it.contains(key)){
                status = true
                return status
            }
        }
        return status

    }

    fun isExistingItemOne(key: String): Boolean{
        var status = false
        selectedListOne.forEach {
            if (it.contains(key)){
                status = true
                return status
            }
        }
        return status

    }



    private fun addNewChip(person: String, chipGroup: ChipGroup) {
        val chip = Chip(this)
        chip.text = person
        chip.isCloseIconEnabled = true
        chip.isClickable = true
        chip.isCheckable = true

        chipGroup.addView(chip as View, chipGroup.childCount - 1)

        chip.setOnCloseIconClickListener {
            chipGroup.removeView(chip as View)
            selectedList.remove(person)
        }
    }

}