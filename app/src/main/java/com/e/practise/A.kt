package com.e.practise

fun main(){
    val T = readLine()!!.toInt()

    repeat(T){
        var s = readLine()!!
        var ans = ""
        var allChars = getUsed(s, s[0])

        for (takeNow in 'z' downTo 'a'){
            if (getUsed(s, takeNow) == allChars){
                ans += takeNow
                val firstIndex = s.indexOfFirst { it == takeNow }
                s = s.substring(firstIndex)
                s = s.replace(takeNow.toString(), "")
            }
        }
        println(ans)
    }

}

fun getUsed(s: String, takeFirst: Char): Int{
    var res = 0
    var seen = false
    s.forEach {
        if (it == takeFirst){
            seen = true
        }
        if (seen){
            res = res or (1 shl (it-'a'))
        }
    }
    return res
}