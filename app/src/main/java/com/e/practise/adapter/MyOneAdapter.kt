package com.e.practise.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.e.practise.Data
import com.e.practise.OnItemClick
import com.e.practise.R
import com.e.practise.contract.OnItemClickOne
import kotlinx.android.synthetic.main.item_layout.view.*


class MyOneAdapter(
    private var distList: ArrayList<Data>,
    private val onItemClick: OnItemClickOne
) :
    RecyclerView.Adapter<MyOneAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return distList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = distList[position].name

        holder.itemView.setOnClickListener {
            onItemClick.onClickOne(distList[position].name!!)
        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView: TextView = itemView.textView
    }

    fun filterList(list: ArrayList<Data>){
        distList = list
        notifyDataSetChanged()
    }

}

