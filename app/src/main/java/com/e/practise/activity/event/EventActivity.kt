package com.e.practise.activity.event

import android.Manifest
import android.app.DatePickerDialog
import android.app.DownloadManager
import android.app.ProgressDialog
import android.content.*
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.CalendarContract.Events
import android.provider.CalendarContract.Reminders
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.webkit.URLUtil
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import com.downloader.*
import com.e.practise.R
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.skydoves.androidribbon.ribbonView
import kotlinx.android.synthetic.main.activity_event.*
import java.io.File
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*

import java.lang.Error


const val URL = "https://www.iitk.ac.in/esc101/share/downloads/javanotes5.pdf"

class EventActivity : AppCompatActivity() {

    private var myDob: Calendar? = Calendar.getInstance()
    private var proDate: DatePickerDialog.OnDateSetListener? = null

    var shake: Animation? = null

    private var fullDate: String? = null

    var downloadId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)

        shake = AnimationUtils.loadAnimation(this, R.anim.shake)

        ivNot?.startAnimation(shake)

        PRDownloader.initialize(applicationContext)


        val startMillis: Long = Calendar.getInstance().run {
            set(2021, 1, 11, 10, 40)
            timeInMillis
        }

        val endMillis: Long = Calendar.getInstance().run {
            set(2021, 1, 11, 11, 40)
            timeInMillis
        }

        btnDownload.setOnClickListener {
            /*var request = DownloadManager.Request(Uri.parse(URL))
                .setTitle("File Name")
                .setDescription("Java Programming Book")
                .setDestinationUri(Uri.fromFile(File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),".pdf")))
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
                .setAllowedOverMetered(true)
            var dm = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            downloadId = dm.enqueue(request)*/

            checkpermission()
        }

        /* var bs = object : BroadcastReceiver(){
             override fun onReceive(context: Context?, intent: Intent?) {
                 var id = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
                 if (id == downloadId){
                     Toast.makeText(this@EventActivity, "Download Completed", Toast.LENGTH_SHORT).show()
                 }
             }
         }

         registerReceiver(bs, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))*/


        btnAddEvent.setOnClickListener {
            /* if (etTitle.text.toString().trim().isNotEmpty() && etDescription.text.toString().trim()
                     .isNotEmpty() && etLocation.text.toString().trim().isNotEmpty()
             ){
                 val intent = Intent(Intent.ACTION_INSERT)
                 intent.setData(Events.CONTENT_URI)
                     .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
                     .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMillis)
                     .putExtra(Events.TITLE, "Lead Status Changed")
                     .putExtra(
                         Events.DESCRIPTION,
                         "Lead Status has been Changed by Mr. SrikantNag."
                     )
                     .putExtra(Events.EVENT_LOCATION, "Office")
                     .putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com")
                 startActivity(intent)
             }else{
                 Toast.makeText(this, "Pls fill all the field.", Toast.LENGTH_SHORT).show()
             }*/
            checkCalendarPermission()
        }

        //ribbon.setBackgroundColor(resources.getColor(R.color.colorYellow))

        /* val ribbonView = ribbonView(this){
             setText("Android-Ribbon")
             setTextColor(Color.WHITE)
             setTextSize(13f)
             setRibbonRotation(-45)
             setRibbonBackgroundColor(ContextCompat.getColor(this@EventActivity,R.color.colorYellow))
         }*/

        rvText.ribbon.apply {
            ribbonBackgroundColor = ContextCompat.getColor(this@EventActivity, R.color.colorYellow)
            text = "Guest"
            textSize = 14f
            setTextColor(Color.BLACK)
        }




        btnSendMsg.setOnClickListener {
            try {
                val sendMsg = Intent(Intent.ACTION_VIEW)
                val url =
                    "https://api.whatsapp.com/send?phone=" + "+91 9734190019 +91 9007673274 +91 9330252212" + "&text=" + URLEncoder.encode(
                        "Hello Testing message",
                        "UTF-8"
                    )
                sendMsg.setPackage("com.whatsapp")
                sendMsg.data = Uri.parse(url)
                if (sendMsg.resolveActivity(packageManager) != null) {
                    startActivity(sendMsg)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        proposeDateClick()

    }

    private fun checkpermission() {
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ).withListener(object : PermissionListener, MultiplePermissionsListener {
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    if (p0?.areAllPermissionsGranted() == true) {
                        downloadFile()
                    } else {
                        checkpermission()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {
                    p1?.continuePermissionRequest()
                }

                override fun onPermissionGranted() {
                    downloadFile()
                }

                override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
                    checkpermission()
                }


            }).check()
    }

    private fun downloadFile() {

        val pd = ProgressDialog(this)
        pd.setMessage("Downloading...")
        pd.setCancelable(false)
        pd.show()

        val file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)

        val downloadId =
            PRDownloader.download(URL, file.path, URLUtil.guessFileName(URL, null, null))
                .build()
                .setOnStartOrResumeListener {

                }
                .setOnPauseListener {

                }
                .setOnCancelListener {

                }
                .setOnProgressListener {
                    val per = it.currentBytes * 100 / it.totalBytes
                    pd.setMessage("Downloading : $per%")

                }
                .start(object : OnDownloadListener {
                    override fun onDownloadComplete() {
                        pd.cancel()
                        Toast.makeText(this@EventActivity, "Download complete", Toast.LENGTH_SHORT)
                            .show()
                    }

                    override fun onError(error: com.downloader.Error?) {
                        Toast.makeText(this@EventActivity, "$error", Toast.LENGTH_SHORT).show()
                    }

                })

    }

    private fun checkCalendarPermission() {
        val permissionListener = object : com.gun0912.tedpermission.PermissionListener {
            override fun onPermissionGranted() {
                createEvent()
                Toast.makeText(this@EventActivity, "success", Toast.LENGTH_SHORT).show()
            }

            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
                checkCalendarPermission()
            }
        }
        TedPermission.with(this@EventActivity)
            .setPermissionListener(permissionListener)
            .setPermissions(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR)
            .check()
    }

    private fun proposeDateClick() {
        proDate = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            myDob!![Calendar.YEAR] = year
            myDob!![Calendar.MONTH] = monthOfYear
            myDob!![Calendar.DAY_OF_MONTH] = dayOfMonth
            updatePolicyDateFormat()
        }
        etDate?.setOnClickListener {
            val date = DatePickerDialog(
                this, proDate,
                myDob!![Calendar.YEAR],
                myDob!![Calendar.MONTH],
                myDob!![Calendar.DAY_OF_MONTH]
            )
            date.datePicker.minDate = System.currentTimeMillis()
            date.show()
        }
    }

    private fun updatePolicyDateFormat() {
        val invoiceDateFormat = "dd MMM,yyyy"
        val sdf = SimpleDateFormat(invoiceDateFormat, Locale.US)
        etDate?.setText(sdf.format(myDob!!.time))
        val sdf1 = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        fullDate = sdf1.format(myDob?.time)
    }

    private fun createEvent() {

        //val response = StringTokenizer(fullDate)

        //val date = fullDate.nextToken().toInt()
        //val month = fullDate.nextToken()
        //val year = fullDate.nextToken().toInt()

        /*val d = response.nextToken()
        val m = response.nextToken()
        val y = response.nextToken()*/

        println("$fullDate")

        val d = fullDate?.substring(0, 2)
        val m = fullDate?.substring(3, 5)
        val y = fullDate?.substring(6, 10)

        var monthValue: Int? = null
        println("date is $d && month is $m && year is $y")


        println("month is $monthValue")


        val sT: Long = Calendar.getInstance().run {
            d?.let { date ->
                getMonth(m)?.let { month ->
                    y?.let { year ->
                        set(
                            year.toInt(),
                            month,
                            date.toInt()
                        )
                    }
                }
            }
            timeInMillis
        }

        val eT: Long = Calendar.getInstance().run {
            set(2021, 1, 24)
            timeInMillis
        }


        val EVENTS_URI = Uri.parse(getCalendarUriBase(true) + "events")

        val event = ContentValues()
        event.put(Events.CALENDAR_ID, 1)
        event.put(Events.TITLE, "Lead Status")
        event.put(Events.DESCRIPTION, "LEAD ID")
        event.put(Events.DTSTART, sT)
        event.put(Events.DTEND, eT)
        event.put(Events.RRULE, "FREQ=YEARLY")
        event.put(Events.ALL_DAY, 1)
        event.put(Events.HAS_ALARM, 1)


        val timeZone = TimeZone.getTimeZone("UTC").id
        event.put(Events.EVENT_TIMEZONE, "UTC/GMT +2:00")


        val e = contentResolver.insert(EVENTS_URI, event)
        Toast.makeText(this, "Event created successfully", Toast.LENGTH_SHORT).show()

        val eventID: Long = e?.lastPathSegment!!.toLong()

        val REMINDERS_URI = Uri.parse(getCalendarUriBase(true) + "reminders")
        val reminderValues = ContentValues()

        reminderValues.put(Reminders.EVENT_ID, eventID)
        reminderValues.put(Reminders.MINUTES, 1)
        reminderValues.put(Reminders.METHOD, Reminders.METHOD_ALERT)

        contentResolver.insert(REMINDERS_URI, reminderValues)
        Toast.makeText(this, "Reminder created successfully", Toast.LENGTH_SHORT).show()

    }

    private fun getMonth(v: String?): Int? {
        var month: Int? = null
        when {
            v.equals("01", true) -> {
                month = 0
            }
            v.equals("02", true) -> {
                month = 1
            }
            v.equals("03", true) -> {
                month = 2
            }
            v.equals("04", true) -> {
                month = 3
            }
            v.equals("05", true) -> {
                month = 4
            }
            v.equals("06", true) -> {
                month = 5
            }
            v.equals("07", true) -> {
                month = 6
            }
            v.equals("08", true) -> {
                month = 7
            }
            v.equals("09", true) -> {
                month = 8
            }
            v.equals("10", true) -> {
                month = 9
            }
            v.equals("11", true) -> {
                month = 10
            }
            v.equals("12", true) -> {
                month = 11
            }

        }

        return month
    }

    private fun getCalendarUriBase(eventUri: Boolean): String {
        var calendarURI: Uri? = null
        try {
            calendarURI = if (Build.VERSION.SDK_INT <= 21) {
                if (eventUri) Uri.parse("content://calendar/") else Uri.parse("content://calendar/calendars")
            } else {
                if (eventUri) Uri.parse("content://com.android.calendar/") else Uri
                    .parse("content://com.android.calendar/calendars")
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return calendarURI.toString()
    }


}