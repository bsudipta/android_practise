package com.e.practise.activity.pushnotification

data class PushNotification(
    val to: String,
    val data: NotificationData
)
