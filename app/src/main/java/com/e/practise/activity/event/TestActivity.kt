package com.e.practise.activity.event

import android.app.Activity
import android.graphics.Rect
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.e.practise.R
import kotlinx.android.synthetic.main.activity_main.*

class TestActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

       /* onFocusChange({
            setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        }) {            setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        }*/
    }

    private fun Activity.onFocusChange(hasFocusAction: () -> Unit, hasNoFocusAction: () -> Unit) {
        view?.viewTreeObserver?.addOnGlobalLayoutListener {
            val r = Rect()
            //r will be populated with the coordinates of your view that area still visible.
            view?.getWindowVisibleDisplayFrame(r)

            val heightDiff = view?.rootView?.height?.minus((r.bottom - r.top))
            if (heightDiff != null) {
                if (heightDiff > 500) { // if more than 100 pixels, its probably a keyboard...
                    hasFocusAction()
                } else {
                    hasNoFocusAction()
                }
            }
        }
    }
}