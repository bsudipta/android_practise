package com.e.practise.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.Animatable
import android.graphics.drawable.BitmapDrawable
import android.media.ExifInterface
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.transition.TransitionManager
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import com.e.practise.R
import com.e.practise.activity.animation.AnimationActivity
import com.e.practise.activity.camera.CameraActivity
import com.e.practise.activity.camera.PhotoActivity
import com.e.practise.activity.event.EventActivity
import com.e.practise.activity.extension.circleExplode
import com.e.practise.activity.location.LocationActivity
import com.e.practise.activity.pushnotification.FirebaseService
import com.e.practise.activity.pushnotification.NotificationData
import com.e.practise.activity.pushnotification.PushNotification
import com.e.practise.activity.pushnotification.RetrofitInstance
import com.e.practise.databinding.ActivityPushNotificationBinding
import com.google.android.material.transition.MaterialArcMotion
import com.google.android.material.transition.MaterialContainerTransform
import com.google.android.material.transition.MaterialFade
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_push_notification.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

const val TOPIC = "/topics/myTopic2"
private const val REQUEST_CODE = 2020
private const val FILE_NAME = "photo.jpg"
class PushNotificationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPushNotificationBinding

    val TAG = "PushActivity"
    private lateinit var photoFile: File

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPushNotificationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        FirebaseService.sharedPref = getSharedPreferences("sharedPref", Context.MODE_PRIVATE)
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            FirebaseService.token = it.token
            etToken.setText(it.token)
            Log.d("token : ", it.token)
        }

        val text: String = "Sunrise Greens HIG Apartment Owners Association"



        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC)

        binding.roomApp.setOnClickListener {

        }

        btnSend.setOnClickListener {
            val title = etTitle.text.toString()
            val message = etMessage.text.toString()
            val recipientToken = etToken.text.toString()
            if (title.isNotEmpty() && message.isNotEmpty() && recipientToken.isNotEmpty()){
                PushNotification(
                    recipientToken,
                    NotificationData(title, message)
                ).also {
                    sendNotification(it)
                }
            }
        }

        btnCreateEvent.setOnClickListener {
            startActivity(Intent(this@PushNotificationActivity, EventActivity::class.java))
        }

        btnAnim?.setOnClickListener {
            startActivity(Intent(this@PushNotificationActivity, AnimationActivity::class.java))
        }

        btnCustomCamera.setOnClickListener {
            startActivity(Intent(this@PushNotificationActivity, PhotoActivity::class.java))
        }

        /**
         * Custom Material Alert Dialog
         * */
       /* btnNext.setOnClickListener {
            showAlert {
                title = getString(R.string.remove_logout_title)
                message = HtmlHelper.fromHtmlToText(
                    getString(R.string.remove_logout_message)
                ).toString()
                positiveButton {
                    text = getString(R.string.delete_btn_text_alert)
                    onClick = {
                        Toast.makeText(this@PushNotificationActivity, "Yes", Toast.LENGTH_SHORT).show()
                    }
                }
                negativeButton {
                    text = getString(R.string.cancel_btn_text_alert)

                }
            }
        }*/


        /**
         * Image capture
         * */

        btnCapture?.isEnabled = false

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 111)
        }else{
            btnCapture?.isEnabled = true
        }

        btnCapture?.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

            photoFile = getPhotoFile(FILE_NAME)


            val fileProvider = FileProvider.getUriForFile(
                this,
                "com.e.practise.fileprovider",
                photoFile
            )
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileProvider)
           /* intent.putExtra("android.intent.extras.CAMERA_FACING", android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT);
            intent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
            intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);*/
            if (intent.resolveActivity(this.packageManager) != null){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                    intent.putExtra(
                        "android.intent.extras.CAMERA_FACING",
                        android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT
                    );
                    intent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1);
                    intent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true);
                } else {
                    intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                    intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                }
                startActivityForResult(intent, REQUEST_CODE)
            }else{
                Toast.makeText(this, "Unable to open camera", Toast.LENGTH_SHORT).show()
            }
        }

        btnNext?.setOnClickListener {
            //startActivity(Intent(this, CameraActivity::class.java))
            startActivity(Intent(this, LocationActivity::class.java))
        }
        //tickAnimation()
        fabAnim()
        binding.fab.isVisible = true

    }
    /**
     * Tick Animation
     */


   /* private fun tickAnimation(){
        binding.ivTick.setImageResource(R.drawable.ic_attendance_tick)
        binding.ivTick.visibility = View.VISIBLE


        if (binding.ivTick.drawable is Animatable) {
            (binding.ivTick.drawable as Animatable).start()
        }
    }
*/
    /**
     * Fab circle explosion Animation.
     * */

    private fun fabAnim(){
        val animation = AnimationUtils.loadAnimation(this, R.anim.circle_explotion).apply {
            duration = 700
            interpolator = AccelerateDecelerateInterpolator()
        }

        binding.fab.setOnClickListener {
            binding.fab.isVisible = false
            binding.circle.isVisible = true
            binding.circle.circleExplode(animation){
                //binding.root.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary))
                binding.circle.isVisible = true
                startActivity(Intent(this@PushNotificationActivity, EventActivity::class.java))
            }
        }

      /*  binding.fab.setOnClickListener {
            val transform: MaterialContainerTransform = MaterialContainerTransform().apply {
                startView = binding.fab
                endView = binding.fabView
                pathMotion = MaterialArcMotion()
                scrimColor = Color.TRANSPARENT
            }
            val materialFade = MaterialFade()
            TransitionManager.beginDelayedTransition(binding.root, materialFade)
            binding.fab.visibility = View.GONE
            binding.fabView.visibility = View.VISIBLE

        }*/


    }

    private fun getPhotoFile(fileName: String): File {
        val storageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(fileName, ".jpg", storageDirectory)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        /*if (requestCode == 101){
            val pic = data?.getParcelableExtra<Bitmap>("data")
            //ivImage.setImageBitmap(pic)
            setImage(pic)
        }*/
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK){
            //val takenImage = data?.extras?.get("data") as Bitmap
                handleRotation(photoFile.absolutePath)
            val takenImage = BitmapFactory.decodeFile(photoFile.absolutePath)
            //ivImage.setImageBitmap(takenImage)
            setImage(takenImage)
        }else{
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun setImage(pic: Bitmap?) {
        pic?.let {
            addStampToImage(it)
        }
        /**
         * Compress and Image quality high
         * */
        val baos = ByteArrayOutputStream()
        val bitmapImage = (ivImage.drawable as BitmapDrawable).bitmap
        val nh = (bitmapImage.height * (256.0 / bitmapImage.width)).toInt()
        val scaled = Bitmap.createScaledBitmap(bitmapImage, 256, nh, true)
        scaled.compress(Bitmap.CompressFormat.PNG, 100, baos)

        val imageInByte: ByteArray = baos.toByteArray()
        /*val fileInputStream = FileInputStream(photoFile.absolutePath)
        val data = fileInputStream.readBytes()*/
        val imageString = Base64.encodeToString(imageInByte, Base64.DEFAULT)
        //text.text = imageString
        println("imagestring : $imageString")

    }

    private fun addStampToImage(originalBitmap: Bitmap) {
        val extraHeight = (originalBitmap.height * 0.15).toInt()
        val newBitmap = Bitmap.createBitmap(
            originalBitmap.width,
            originalBitmap.height + extraHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(newBitmap)
        canvas.drawBitmap(originalBitmap, 0f, 0f, null)
        canvas.drawColor(Color.parseColor("#64000000"))
        val resources: Resources = resources
        val scale: Float = resources.displayMetrics.density
        val pText = Paint()
        pText.color = Color.parseColor("#FFEB3B")
        pText.textSize = (40 * scale).toInt().toFloat()
        //pText.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
        //pText.typeface = Typeface.create(Typeface.createFromAsset(this.assets,"font/poppins_bold.ttf"), Typeface.BOLD)
        //
        val sdf = SimpleDateFormat("dd MMM, yyyy")
        val sdfTime = SimpleDateFormat("hh:mm:ss")
        val date = sdf.format(Date())
        val time = sdfTime.format(Date())
        val userName = "UserName : Test"
        val currentDate = "Date : $date"
        val currentTime = "Time : $time"

        val bounds = Rect()
        pText.getTextBounds(userName, 0, userName.length, bounds)
        val x = (newBitmap.width - bounds.width()) / 6
        val y = (newBitmap.height + bounds.height()) / 4
        val y1 = (newBitmap.height + bounds.height()) / 4.6f
        val y2 = (newBitmap.height + bounds.height()) / 4.3f
        //val y = (Math.abs(bounds.height()))/2
        canvas.drawText(currentTime, x * scale, y * scale, pText)
        canvas.drawText(userName, x * scale, y1 * scale, pText)
        canvas.drawText(currentDate, x * scale, y2 * scale, pText)
        ivImage.setImageBitmap(newBitmap)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 111 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            btnCapture?.isEnabled = true
        }
    }

    private fun handleRotation(imgPath: String) {
        BitmapFactory.decodeFile(imgPath)?.let { origin ->
            try {
                ExifInterface(imgPath).apply {
                    getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED
                    ).let { orientation ->
                        when (orientation) {
                            ExifInterface.ORIENTATION_ROTATE_90 -> origin.rotate(90f)
                            ExifInterface.ORIENTATION_ROTATE_180 -> origin.rotate(180f)
                            ExifInterface.ORIENTATION_ROTATE_270 -> origin.rotate(270f)
                            ExifInterface.ORIENTATION_NORMAL -> origin
                            else -> origin                  //origin.rotate(270f)
                        }.also { bitmap ->
                            //Update the input file with the new bytes.
                            try {
                                FileOutputStream(imgPath).use { fos ->
                                    bitmap.compress(
                                        Bitmap.CompressFormat.JPEG,
                                        100,
                                        fos
                                    )
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun Bitmap.rotate(degrees: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degrees)
        val scaledBitmap = Bitmap.createScaledBitmap(this, width, height, true)
        return Bitmap.createBitmap(
            scaledBitmap,
            0,
            0,
            scaledBitmap.width,
            scaledBitmap.height,
            matrix,
            true
        )
    }


    private fun sendNotification(notification: PushNotification) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val response = RetrofitInstance.api.pushNotification(notification)
            if (response.isSuccessful){
                //Log.e(TAG, "Response: ${Gson().toJson(response)}")
                Toast.makeText(baseContext, "Success", Toast.LENGTH_SHORT).show()
            }else{
                Log.e(TAG, response.errorBody().toString())
            }

        }catch (e: Exception){
            Log.e(TAG, e.toString())
        }
    }

}