package com.e.practise.activity.camera

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.Resources
import android.graphics.*
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.e.practise.R
import com.yalantis.ucrop.UCrop
import kotlinx.android.synthetic.main.activity_image.*
import java.io.File
import java.io.FileDescriptor
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class ImageActivity : AppCompatActivity() {

    private var file: String? = null
    var imageUri: Uri? = null

    @SuppressLint("WrongThread")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)


        imageUri = intent.getStringExtra("imageLink")?.toUri()

        imageUri?.let { startCrop(it) }

        file = intent.getStringExtra("image")

        //image.setImageBitmap(modifyOrientation(BitmapFactory.decodeFile(file),file))
        //addStampToImage(BitmapFactory.decodeFile(file))

        //modifyOrientation(BitmapFactory.decodeFile(file),file)?.let { addStampToImage(it) }

      /*  val baos = ByteArrayOutputStream()
        val bitmapImage = (image.drawable as BitmapDrawable).bitmap
        val nh = (bitmapImage.height * (256.0 / bitmapImage.width)).toInt()
        val scaled = Bitmap.createScaledBitmap(bitmapImage, 256, nh, true)
        scaled.compress(Bitmap.CompressFormat.PNG, 100, baos)

        val imageInByte: ByteArray = baos.toByteArray()

        val imageString = "data:image/jpeg;base64," + Base64.encodeToString(imageInByte, Base64.DEFAULT)

        println("Image is $imageString")*/


       // image.setImageURI(file)

       // val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, file)
        //flip(bitmap)?.let { addStampToImage(it) }
        //addStampToImage(bitmap)

        //image.setImageBitmap(applyWaterMarkEffect(bitmap,"This is the test watermark text.",200,200, Color.GREEN,80,24,false))
        //file?.let { handleRotation(it) }

        //image.setImageURI(file)

    }

    private fun startCrop(uri: Uri){
        var destinationFileName = "Sample Crop Image"
        destinationFileName += ".jpg"

        val uCrop = UCrop.of(uri, Uri.fromFile(File(cacheDir, destinationFileName)))
        uCrop.withAspectRatio(1F, 1F)
        uCrop.withMaxResultSize(450, 450)
        uCrop.withOptions(getCropOption())
        uCrop.start(this@ImageActivity)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP){
            val uri = data?.let { UCrop.getOutput(it) }
            //image.setImageURI(uri)
            //modifyOrientation(BitmapFactory.decodeFile(uri.toString()),uri.toString())?.let { addStampToImage(it) }
            //val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)
            val bitmap = uri?.let { getBitmapFromUri(it) }
            bitmap?.let { addStampToImage(it) }
        }
    }

    @Throws(IOException::class)
    private fun getBitmapFromUri(uri: Uri): Bitmap? {
        val parcelFileDescriptor: ParcelFileDescriptor =
            this.contentResolver.openFileDescriptor(uri, "r")!!
        val fileDescriptor: FileDescriptor = parcelFileDescriptor.fileDescriptor
        val image = BitmapFactory.decodeFileDescriptor(fileDescriptor)
        parcelFileDescriptor.close()
        return image
    }

    private fun getCropOption(): UCrop.Options{
        val options = UCrop.Options()
        options.setCompressionQuality(70)
        //compress type
        //options.setCompressionFormat(Bitmap.CompressFormat.PNG)

        //UI
        options.setHideBottomControls(false)
        options.setFreeStyleCropEnabled(true)

        //Colors
        options.setStatusBarColor(resources.getColor(R.color.colorPrimaryDark))
        options.setToolbarColor(resources.getColor(R.color.colorPrimary))

        options.setToolbarTitle("Crop Activity")

        return options
    }






    @Throws(IOException::class)
    fun modifyOrientation(bitmap: Bitmap, image_absolute_path: String?): Bitmap? {
        val ei = ExifInterface(image_absolute_path!!)
        return when (ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_NORMAL
        )) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotate(bitmap, 90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotate(bitmap, 180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotate(bitmap, 270f)
            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> flip(
                bitmap,
                horizontal = true,
                vertical = false
            )
            ExifInterface.ORIENTATION_FLIP_VERTICAL -> flip(
                bitmap,
                horizontal = false,
                vertical = true
            )
            else -> bitmap
        }
    }

    private fun rotate(bitmap: Bitmap, degrees: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(degrees)
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    private fun flip(bitmap: Bitmap, horizontal: Boolean, vertical: Boolean): Bitmap? {
        val matrix = Matrix()
        matrix.preScale(
            (if (horizontal) -1 else 1.toFloat()) as Float,
            (if (vertical) -1 else 1.toFloat()) as Float
        )
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }


    private fun addStampToImage(originalBitmap: Bitmap) {
        val extraHeight = (originalBitmap.height * 0.15).toInt()
        val newBitmap = Bitmap.createBitmap(
            originalBitmap.width,
            originalBitmap.height + extraHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(newBitmap)
        canvas.drawBitmap(originalBitmap, 0f, 0f, null)
        canvas.drawColor(Color.parseColor("#64000000"))
        val resources: Resources = resources
        val scale: Float = resources.displayMetrics.density
        val pText = Paint()
        pText.color = Color.parseColor("#FFEB3B")
        pText.textSize = (10 * scale).toInt().toFloat()
        //pText.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
       /* pText.typeface =
            Typeface.create(Typeface.createFromAsset(requireContext().assets,"font/poppins_bold.ttf"), Typeface.BOLD)*/
        //
        val sdf = SimpleDateFormat("dd MMM, yyyy")
        val sdfTime = SimpleDateFormat("hh:mm:ss")
        val date = sdf.format(Date())
        val time = sdfTime.format(Date())
        val userName = "UserName : Test"
        val currentDate = "Date : $date"
        val currentTime = "Time : $time"

        val bounds = Rect()
        pText.getTextBounds(userName, 0, userName.length, bounds)
        val x = (newBitmap.width - bounds.width()) / 6
        val y = (newBitmap.height + bounds.height()) / 4
        val y1 = (newBitmap.height + bounds.height()) / 4.6f
        val y2 = (newBitmap.height + bounds.height()) / 4.3f
        //val y = (Math.abs(bounds.height()))/2
        canvas.drawText(currentTime, x * scale, y * scale, pText)
        canvas.drawText(userName, x * scale, y1 * scale, pText)
        canvas.drawText(currentDate, x * scale, y2 * scale, pText)
        image.setImageBitmap(newBitmap)
    }

    fun flip(src: Bitmap): Bitmap? {
        // create new matrix for transformation
        val matrix = Matrix()
        matrix.preScale(-1.0f, 1.0f, -1.0f, 1.0f)

        // return transformed image
        return Bitmap.createBitmap(src, 0, 0, src.width, src.height, matrix, true)
    }




}