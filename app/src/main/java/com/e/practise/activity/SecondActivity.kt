package com.e.practise.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import com.e.practise.R
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.filter_chip.*

class SecondActivity : AppCompatActivity() {

    private val PEOPLE = arrayOf("John Smith", "Kate Eckhart", "Emily Sun", "Frodo Baggins")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.filter_chip)

        val adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_dropdown_item_1line,
            PEOPLE
        )

        autoCompleteTextView?.setAdapter(adapter)
        autoCompleteTextView?.setOnItemClickListener { adapterView, view, i, l ->

            val selected = adapterView.getItemAtPosition(i) as String
            addChipToGroup(selected,chipGroup2)
        }

    }

    private fun addChipToGroup(person: String, chipGroup: ChipGroup) {
        val chip = Chip(this)
        chip.text = person
        chip.isCloseIconEnabled = true


        // necessary to get single selection working
        chip.isClickable = true
        chip.isCheckable = false
        chipGroup.addView(chip as View)
        chip.setOnCloseIconClickListener { chipGroup.removeView(chip as View) }
    }



}