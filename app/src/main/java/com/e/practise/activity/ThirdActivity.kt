package com.e.practise.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.arges.sepan.argmusicplayer.Enums.AudioType
import com.arges.sepan.argmusicplayer.IndependentClasses.ArgAudio
import com.arges.sepan.argmusicplayer.PlayerViews.ArgPlayerLargeView
import com.e.practise.R
import com.e.practise.adapter.ThirdListAdapter
import com.e.practise.network.ApiService
import com.e.practise.viewmodel.ThirdViewModel
import com.e.practise.viewmodel.ThirdViewModelFactory
import com.vatsal.imagezoomer.ZoomAnimation
import kotlinx.android.synthetic.main.third_layout.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class ThirdActivity : AppCompatActivity() {

    private lateinit var viewModel: ThirdViewModel
    private lateinit var thirdListAdapter: ThirdListAdapter

    private lateinit var audioPlayer: ArgPlayerLargeView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.second_layout)
        setContentView(R.layout.third_layout)

        img.setOnClickListener {
            val zoomAnimation = ZoomAnimation(this)
            zoomAnimation.zoom(it,3000)
        }


       /* val wordtoSpan: Spannable =
            SpannableString("Existing User? Login")
        wordtoSpan.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorDarkGray)),
            0,
            14,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        wordtoSpan.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorOrange)),
            16,
            20,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        textView.text = wordtoSpan

        val dataStore: DataStore<androidx.datastore.preferences.Preferences> = this.createDataStore(name = "sss")*/

       /* setUpViewModel()
        setUpList()
        setUpView()*/
        voicePlayerView.setSeekBarStyle(R.color.colorPrimaryDark,R.color.colorOrange)
        voicePlayerView.setAudio("https://envol.app/uploads/contents/20201029013605_Rest and Repair final meditation FR 25 min.mp3.mp3")


    }

    private fun setUpView(){
        lifecycleScope.launch {
            viewModel.listData.collect {
                thirdListAdapter.submitData(it)
            }
        }
    }

    private fun setUpList(){
        thirdListAdapter = ThirdListAdapter()
        rvList?.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = thirdListAdapter
        }
    }

    private fun setUpViewModel(){
        viewModel = ViewModelProvider(
            this,
            ThirdViewModelFactory(ApiService.getApiServices())
        )[ThirdViewModel::class.java]

    }
}