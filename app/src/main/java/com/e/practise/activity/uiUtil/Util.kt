package com.e.practise.activity.uiUtil

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.text.Html
import android.text.Spanned

object HtmlHelper{
    fun fromHtmlToText(html: String?): Spanned? {
        return if (html != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
            } else{
                Html.fromHtml(html)
            }
        }else null
    }
}

/**
 * This method is responsible for making Bitmap from Uri
 */
fun getBitmap(context: Context, imageUri: Uri): Bitmap? {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
        ImageDecoder.decodeBitmap(
            ImageDecoder.createSource(
                context.contentResolver,
                imageUri
            )
        )
    } else {
        context.contentResolver.openInputStream(imageUri)?.use { inputStream ->
            BitmapFactory.decodeStream(inputStream)
        }
    }
}