package com.e.practise.activity.pushnotification

data class NotificationData(
    val title: String,
    val message: String
)
