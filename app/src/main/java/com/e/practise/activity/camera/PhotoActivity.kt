package com.e.practise.activity.camera

import android.Manifest
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaActionSound
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Rational
import android.util.Size
import android.widget.Toast
import androidx.camera.core.*
import androidx.camera.core.impl.PreviewConfig
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import com.e.practise.R
import com.e.practise.databinding.PhotoLayoutBinding
import com.gun0912.tedpermission.TedPermission
import kotlinx.android.synthetic.main.photo_layout.*
import java.io.File

class PhotoActivity : AppCompatActivity() {

    private lateinit var binding: PhotoLayoutBinding

    var camera: Camera? = null
    var preview: Preview? = null
    var imageCapture: ImageCapture? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = PhotoLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)


        //val rotation = this.windowManager.defaultDisplay.rotation

        checkPermissionOne()

        binding.captureImageButton.setOnClickListener {
            takePicture()
        }


    }

    private fun checkPermissionOne() {
        val permissionListener = object : com.gun0912.tedpermission.PermissionListener {
            override fun onPermissionGranted() {
                //toast("permission granted")
                startCamera()
            }

            override fun onPermissionDenied(deniedPermissions: MutableList<String>?) {
                Toast.makeText(this@PhotoActivity, "failed", Toast.LENGTH_SHORT).show()
            }
        }
        TedPermission.with(this)
            .setPermissionListener(permissionListener)
            .setPermissions(Manifest.permission.CAMERA)
            .check()
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener(Runnable {
            val cameraProvider = cameraProviderFuture.get()
            preview = Preview.Builder().build()
            preview?.setSurfaceProvider(binding.previewView.surfaceProvider)
            imageCapture = ImageCapture.Builder().build()
            val cameraSelector =
                CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_FRONT).build()
            cameraProvider.unbindAll()
            camera = cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)
        }, ContextCompat.getMainExecutor(this))
    }

    private fun takePicture() {
        val photoFile =
            File(externalMediaDirs.firstOrNull(), "CameraApp - ${System.currentTimeMillis()}.jpg")
        val output = ImageCapture.OutputFileOptions.Builder(photoFile).build()
        imageCapture?.takePicture(
            output,
            ContextCompat.getMainExecutor(this),
            object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                    Toast.makeText(
                        this@PhotoActivity,
                        "Image Saved - ${photoFile.absoluteFile}",
                        Toast.LENGTH_SHORT
                    ).show()
                    shutterSound()
                    val i = Intent(this@PhotoActivity, ImageActivity::class.java)
                    i.putExtra("image", photoFile.absolutePath)
                    i.putExtra("imageLink", Uri.fromFile(photoFile).toString())
                    startActivity(i)
                }

                override fun onError(exception: ImageCaptureException) {

                }

            })
    }

    fun shutterSound() {
        val audio = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        when (audio.ringerMode) {
            AudioManager.RINGER_MODE_NORMAL -> {
                val sound = MediaActionSound()
                sound.play(MediaActionSound.SHUTTER_CLICK)
            }
            AudioManager.RINGER_MODE_SILENT -> {

                val sound = MediaActionSound()
                sound.play(MediaActionSound.SHUTTER_CLICK)

            }
            AudioManager.RINGER_MODE_VIBRATE -> {

                val sound = MediaActionSound()
                sound.play(MediaActionSound.SHUTTER_CLICK)
            }
        }
    }



}