package com.e.practise.activity.uiUtil

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder

data class AlertDialogData(
    var title: String? = null,
    var message: String? = null,
    var positive: AlertDialogButtonListener? = null,
    var negative: AlertDialogButtonListener? = null
)

data class AlertDialogButtonListener(
    var text: String? = null,
    var onClick: () -> Unit? = { null }
)

fun AlertDialogData.positiveButton(block: AlertDialogButtonListener.() -> Unit){
    positive = AlertDialogButtonListener().apply(block)
}

fun AlertDialogData.negativeButton(block: AlertDialogButtonListener.() -> Unit){
    negative = AlertDialogButtonListener().apply(block)
}

fun Fragment.showAlert(block: AlertDialogData.() -> Unit){
    val alertDialogData = AlertDialogData().apply(block)
    context?.apply { showAlertDialog(this,alertDialogData) }
}

fun AppCompatActivity.showAlert(block: AlertDialogData.() -> Unit) {
    val alertDialogData = AlertDialogData().apply(block)
    showAlertDialog(this, alertDialogData)
}

private fun showAlertDialog(context: Context?, alertDialogData: AlertDialogData) {
    context?.let {
        MaterialAlertDialogBuilder(it)
            .setTitle(alertDialogData.title)
            .setMessage(alertDialogData.message)
            .setCancelable(false)
            .setPositiveButton(alertDialogData.positive?.text) { _, _ ->
                alertDialogData.positive?.onClick?.let { it() }
            }
            .setNegativeButton(alertDialogData.negative?.text) { _, _ ->
                alertDialogData.negative?.onClick?.let { it() }
            }
            .show()
    }
}