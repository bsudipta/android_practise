package com.e.practise.activity.event

import android.util.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun main(){
    val result = UserValidate().getValidName(User(1,"Sudipta"))
    println("$result")

    val r = UserValidate().getSort(mutableListOf(8,10,4,3,21,1))
    print(r)


    /****
     *
     * */

    val time = parse("2021-09-27T14:06:33.000Z", TIME_PATTERN_DEFAULT)
    println("time is" + time)


    /*val list = listOf(1,2,3,4,5,6,7,8,9).map {
        "Hello $it"
        if (it <= 5){
            "Hello $it"
        }else{
            "Hi $it"
        }
    }*/
    val list = listOf("Sudipta","Abhijit").reduce { acc, i ->
        "$acc $i"
    }
    var inTime: String? = null
    var outTime: String? = null
    inTime = "11:23 AM"
    var finalTime = ""
    inTime?.let {
        outTime += it
    }

    outTime?.let {
        outTime += it
    }

    println("time is $outTime")

    println("Current list : $list")

    val numList = listOf(1,2,3,4,5,6,7,8,9)
    //val chunkList = numList.chunked(3)
    val chunkList = numList.associateBy {
        it.toString()
    }
    println(chunkList)
}

private const val TIME_PATTERN_DEFAULT = "hh:mm a"
private const val DATE_PATTERN_DEFAULT = "yyyy-MM-dd"
private const val DATE_PATTERN_API_DEFAULT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

fun parse(
    date: String,
    pattern: String = TIME_PATTERN_DEFAULT,
    apiPattern: String = DATE_PATTERN_API_DEFAULT
): String? {
    var formattedDate: String? = null
    val parseDate: Date
    try {
        val ssd = SimpleDateFormat(apiPattern, Locale.getDefault())
        ssd.timeZone = TimeZone.getTimeZone("UTC")
        parseDate =
            ssd.parse(date)
        val time = parseDate.time
        formattedDate = SimpleDateFormat(pattern, Locale.getDefault()).format(time)
    } catch (e: ParseException) {
        e.printStackTrace()
    } finally {
        return formattedDate
    }
}

class UserValidate{

    fun <T> getValidName(data: T): List<T>{
        return listOf(data)
    }

    fun <T> getSort(items: MutableList<T>) : List<T>{

        if (items.isEmpty() || items.size<2){
            return items
        }
        for (count in 1 until items.count()){
            // println(items)
            val item = items[count]
            var i = count
            while (i>0 && item.toString() < items[i - 1].toString()){
                items[i] = items[i - 1]
                i -= 1
            }
            items[i] = item
        }
        return items
    }
}