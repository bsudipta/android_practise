package com.e.practise.module


lateinit var volume: Volume
fun main() {
    println(
        "-----\nTurn on volume level to:\n" +
                "high,medium or low?\n" +
                "----"
    )
    changeVolume()
}

private fun changeVolume() {
    val volumeLevel = readLine()
    volume = when {
        volumeLevel!!.contains("low", true) -> {
            Volume.Low("low")
        }
        volumeLevel.contains("medium", true) -> {
            Volume.medium("medium")
        }
        else -> Volume.high("high")
    }

    eval(volume)
    println("----\nTo adjust it type:\nhigh, medium or low.\n----")
    changeVolume()
}

sealed class Volume {
    class Low(var msg: String) : Volume()
    class medium(var msg: String) : Volume()
    class high(var msg: String) : Volume()
}

fun eval(volume: Volume) = when (volume) {
    is Volume.Low -> println("volume set to low + ${volume.msg}")
    is Volume.medium -> println("volume set to medium + ${volume.msg}")
    is Volume.high -> println("volume set to high + ${volume.msg}")
}