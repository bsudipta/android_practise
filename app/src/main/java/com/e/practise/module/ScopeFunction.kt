package com.e.practise.module

fun main() {

    /**
     * let function
     * */

    val name: String? = "hello"

    val s = name.run {
        println(this!!.reversed())
        println(this.capitalize())
        this.length
    }

    println(s)

}