package com.e.practise.module

fun main(){

    val list = mutableListOf("D","A")
    list.add("G")
    //list[0] = "sam"
    //list.removeAt(0)
    //list.shuffle()
    list.sort()
    for (i in list){
        println(i)
    }

    list.forEachIndexed { index, s ->
        if (index == 0){
            println(s)
        }
    }

    val lis = arrayOf(
        arrayOf(1,2,3),
        arrayOf(2,3,4)
    )
    for (i in lis.indices){
        //println(i)
    }

    val s = "sudipto"
    val l = s.split("")
    for (i in l){
        //println("char is $i")
    }

    val vehicles = listOf("1","2","3")

    val manuList = vehicles.flatMap {s ->
        s.toList()
    }

    println("$manuList")

}