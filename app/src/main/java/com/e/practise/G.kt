package com.e.practise

import kotlin.math.cos
import kotlin.math.min

class DJS(n: Int) {
    private var par: MutableList<Int> = MutableList<Int>(n) { -1 }

    fun find(i: Int): Int {
        if (par[i] < 0) {
            return par[i]
        } else {
            par[i] = find(par[i])
            return par[i]
        }
    }

    fun join(ii: Int, jj: Int): Boolean {
        val i = find(ii)
        val j = find(jj)
        if (i == j) return false
        if (par[i] == par[j]) par[i]--
        if (par[i] <= par[j]) {
            par[j] = i
        } else {
            par[i] = j
        }
        return true
    }

}

data class Edge(val from: Int, val to: Int, val cost: Int)

fun main() {
    val (n, m) = readLine()!!.split(" ").map { it.toInt() }
    val a = readLine()!!.split(" ").map { it.toInt() }
    var mIndex = 0
    for (i in 0 until n)
        if (a[i] < a[mIndex]) mIndex = i
    val edges = mutableListOf<Edge>()
    repeat(m){
        val (from,to,cost) = readLine()!!.split(" ").map { it.toInt() }
        edges.add(Edge(from, to, cost))
    }

    for (i in 0 until n)
        edges.add(Edge(i, mIndex, a[mIndex]+a[i]))
    edges.sortBy { it.cost }

}