package com.e.practise

import android.util.Patterns


data class Data(
    val name: String? = null
)


fun getList() = arrayListOf(
    Data("America"),
    Data("Avalla"),

)

fun main() {
   // isValidMobile("999999999")
    //println("${isValidMobile("8888888888")}")

    val story: String = "I am a programmer...!!"
    val story1: String? = null
    story.let {
        println("inside let")
    }
    println("outside let")

}

fun isValidMobile(phone: String): Boolean {
    return Patterns.PHONE?.matcher(phone)?.matches()!!
}